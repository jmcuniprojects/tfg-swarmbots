//
// Created by Jorge on 10/07/2021.
//

#ifndef SOCIALBENCH_CONFIG_H
#define SOCIALBENCH_CONFIG_H

#include <iostream>
#include <vector>
#include <fstream>

class Config
{
private:
    int usuarios;
    int maxEventos;
    int tiempoSim;
    float actividadMedia;
    int etiquetasMedias;
    float probabilidadEvento;
    std::string url;
    std::string ip;
    int tiempoEntEvento;
    int factorDisminucion;
    int segundosEntreMediciones;
public:
    Config(){};
    Config(std::string file);
    Config(Config& orig);
    int getUsuarios() const;
    void setUsuarios(int usuarios);
    int getMaxEventos() const;
    void setMaxEventos(int maxEventos);
    int getTiempoSim() const;
    void setTiempoSim(int tiempoSim);
    float getActividadMedia() const;
    void setActividadMedia(float actividadMedia);
    int getEtiquetasMedias() const;
    void setEtiquetasMedias(int etiquetasMedias);
    float getProbabilidadEvento() const;
    void setProbabilidadEvento(float probabilidadEvento);
    const std::string &getUrl() const;
    void setUrl(const std::string &url);
    const std::string &getIp() const;
    void setIp(const std::string &ip);
    int getTiempoEntEvento() const;
    void setTiempoEntEvento(int timepoEntEvento);
    int getFactorDisminucion() const;
    void setFactorDisminucion(int factorDisminucion);
    int getSegundosEntreMediciones() const;
    void setSegundosEntreMediciones(int segundosEntreMediciones);
};

#endif //SOCIALBENCH_CONFIG_H

//
// Created by Jorge on 23/06/2021.
//
#ifndef SOCIALBENCH_EVENTO_H
#define SOCIALBENCH_EVENTO_H

#pragma once

#include <iostream>
#include <list>
#include <iterator>
#include <stack>
#include <algorithm>
#include <string>
#include "extra.h"
#include "Usuario.h"
#include "ThreadBuffer.h"

class GestorEventos;

class Evento: public Stoppable
{
public:
    Evento(GestorEventos *g, std::default_random_engine seed, int factorDisminucion, ThreadBuffer<std::list<std::string>> *Log);
	~Evento();

	//Metodos de ejecucion usuales
	void ejecutaEvento();
	void guardaEvento(std::list<std::string>& registro);
	void join();

	int getId();
	void setId(int nuevo);
	std::list<Etiquetas> getEtiquetas();
	float getActividad();
	void setActividad(float nuevo);
	std::list<Usuario*> getParticipantes();
	int getNumPosts();
	void setNumPosts(int nuevo);
	bool haTerminado();

	//Metodos de listas
	bool addEtiqueta(Etiquetas nuevo, bool check);
	bool deleteEtiqueta(Etiquetas nuevo);
	bool deleteEtiqueta(int pos);
	std::list<Etiquetas> resetEtiquetas();

	bool addParticipante(Usuario* nuevo, bool check);
	bool deleteParticipante(Usuario* nuevo);
	bool deleteParticipante(int pos);
	std::list<Usuario*> resetParticipantes();
private:
    GestorEventos *gestor;
	int id; //< identinficador unico para el evento
	std::list<Etiquetas> etiquetas; //< temas que se tratan en el evento
	float actividad; //< probabilidad de actuar los usuarios afines
	std::list<Usuario*> participantes; //< Usuarios que participan en el evento
	int numPosts; //< numero de posts hechos hasta el momento
	bool finalizado; //< si ha terminado su ejecucion o no

	//Variables de control del Hilo
	std::thread hiloEjecucion;
	std::random_device rd;
	std::default_random_engine seed;
	std::uniform_int_distribution<int> dist_cien;
	std::uniform_int_distribution<int> dist_cuatro;
	int factorDisminucion;
	ThreadBuffer<std::list<std::string>>* log;

	void run();
};

#endif
//
// Created by Jorge on 21/07/2021.
//

#ifndef SOCIALBENCH_GESTOREVENTOS_H
#define SOCIALBENCH_GESTOREVENTOS_H

#include <iostream>
#include <thread>
#include <chrono>
#include <fstream>

#include "Usuario.h"
#include "Evento.h"
#include "Config.h"
#include "extra.h"
#include "ThreadBuffer.h"

class GestorEventos: public Stoppable {
private:
    std::default_random_engine seed;
    std::uniform_int_distribution<int> dist_cien;
    std::uniform_int_distribution<int> dist_cuatro;
    uint16_t idEventos;

    Config configuracion;
    std::vector<Evento*> eventos;
    std::vector<Usuario*> usuarios;
    int maxEventos;
    ThreadBuffer<std::list<std::string>>* log;
    float mediaInteracciones;
    int totalEventos;
public:
    GestorEventos(Config &conf, std::vector<Usuario*> &usu, int eventosMax, std::default_random_engine seed, ThreadBuffer<std::list<std::string>> *Log);
    void run();
    float getMediaInteracciones();
    float getTotalInteracciones();
};


#endif //SOCIALBENCH_GESTOREVENTOS_H

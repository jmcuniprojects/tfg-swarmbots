//
// Created by jtort on 13/08/2021.
//

#ifndef SOCIALBENCH_LOGWRITER_H
#define SOCIALBENCH_LOGWRITER_H

#include <iostream>
#include <fstream>
#include <list>
#include "extra.h"
#include "ThreadBuffer.h"

class LogWriter: public Stoppable {
private:
    std::string fileName;
    std::ofstream outputFile;
    ThreadBuffer<std::list<std::string>>* buffer;
public:
    LogWriter(ThreadBuffer<std::list<std::string>>* buffer, std::string fileNames);
    void run();
};

#endif //SOCIALBENCH_LOGWRITER_H

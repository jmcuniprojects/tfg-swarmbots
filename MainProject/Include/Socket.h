//
// Created by jtort on 15/08/2021.
//

#ifndef SOCIALBENCH_SOCKET_H
#define SOCIALBENCH_SOCKET_H

#include <stdio.h>
#include <iostream>
#include <cstring>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>

namespace serversock {
    void createConnection(std::string IP);
    std::string receive(int buffer_size);
    void sending(std::string msg);
}

#endif //SOCIALBENCH_SOCKET_H

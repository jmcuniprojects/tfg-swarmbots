//
// Created by jtort on 11/08/2021.
//

#ifndef SOCIALBENCH_THREADBUFFER_H
#define SOCIALBENCH_THREADBUFFER_H

#include <thread>
#include <mutex>
#include "extra.h"
#include <queue>

template <class T>
class ThreadBuffer{
private:
    std::queue<T> buffer; //< cola con los datos
    std::mutex lock; //< exclusion mutua para la cola
    Semaphore cv; //< Bloquea la lectura
public:
    ThreadBuffer();
    void write_buffer(T item);
    bool read_buffer(T& value);
    bool notify_empty();
};

template <class T>
ThreadBuffer<T>::ThreadBuffer():
        cv(0)
{
}

template <class T>
void ThreadBuffer<T>::write_buffer(T item){
    this->lock.lock();
    bool vacio = this->buffer.empty();
    this->buffer.push(item);
    this->lock.unlock();
    if(vacio && this->cv.isWaiting()){
        this->cv.notify(1);
    }
}

template <class T>
bool ThreadBuffer<T>::read_buffer(T& value){
    this->lock.lock();
    if(this->buffer.empty()){
        this->lock.unlock();
        this->cv.wait(1);
        if(this->buffer.empty()){
            return false;
        }
        this->lock.lock();
    }
    value = this->buffer.front();
    this->buffer.pop();
    this->lock.unlock();
    return true;
}

template <class T>
bool ThreadBuffer<T>::notify_empty(){
    if(this->buffer.empty()){
        this->cv.notify(1);
        return true;
    }
    return false;
}

#endif //SOCIALBENCH_THREADBUFFER_H

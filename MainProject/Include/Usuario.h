//
// Created by Jorge on 21/06/2021.
//
#ifndef SOCIALBENCH_USUARIO_H
#define SOCIALBENCH_USUARIO_H

#pragma once

#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>
#include <mastodonpp/mastodonpp.hpp>
#include "extra.h"

class Usuario{
public:
    Usuario(std::string token);
    Usuario();
    virtual ~Usuario();

    mastodonpp::Instance instancia;
    mastodonpp::Connection *conexion;

    //Metodos de ejecucion usuales

    bool registrar();
    void newToken(std::string token);
    bool post(std::string msg);

    //Get y Set
    Genero getGenero();
    void setGenero(Genero nuevo);
    int getEdad();
    void setEdad(int nuevo);
    uint16_t getId();
    void setId(uint16_t nuevo);
    std::string getCorreo();
    void setCorreo(std::string nuevo);
    std::string getPass();
    bool setPass(std::string nuevo, std::string old);

    float getImportancia();
    void setImportancia(float nuevo);
    std::list<Usuario*> getSeguidores();
    bool getPerfil();
    void setPerfil(bool nuevo);
    std::list<Etiquetas> getGustos();
    std::string getToken();

    //Metodos de listas
    bool addSeguidor(Usuario* nuevo, bool check);
    bool deleteSeguidor(Usuario* nuevo);
    bool deleteSeguidor(int pos);
    std::list<Usuario*> resetSeguidores();

    bool addGusto(Etiquetas nuevo, bool check);
    bool deleteGusto(Etiquetas nuevo);
    bool deleteGusto(int pos);
    std::list<Etiquetas> resetGustos();
    bool findGusto(Etiquetas f);
    bool findGusto(std::list<Etiquetas> f);
private:
    Genero gen; //< Genero del usuario
    int edad; //< Edad del usuario
    uint16_t id; //< id unico del usuario
    std::string correo; //< correo del usuario para el log in
    std::string pass; //< contrasena delusuario para log in

    std::string tokenAcceso; //< token para conectar con mastodon

    float importancia; //< influencia del usuario
    std::list<Usuario*> seguidores; //< usuarios que siguen a este usuario
    bool perfil; //< Si es un usuario activo(true) o pasivo(false)
    std::list<Etiquetas> gustos; //< etiquetas afines del usuario
};

#endif //SOCIALBENCH_USUARIO_H

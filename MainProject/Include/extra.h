#pragma once

#include <iostream>
#include <thread>
#include <assert.h>
#include <chrono>
#include <future>
#include <random>
#include <mutex>
#include <condition_variable>
#include <list>
#include <string>


const std::string MASTODON_URL = "ottoand.ddns.net";
static const std::string USERPASS = "accountsDB.csv";
static const int OSCILACION_ETI = 3;
static const int probEtiquetas[4][6] = {{0,10,40,60,90,100},{0,33,66,100,100,100},{0,25,50,75,100,100},{0,33,66,100,100,100}};

enum class Etiquetas
{
    Politica=0,
    ExtremoIzquierda=1,
    Izquierda=2,
    Centro=3,
    Derecha=4,
    ExtremoDerecha=5,

	Ocio=10,
    Deportes=11,
    Videojuegos=12,
    Cine=13,

	Ciencias=20,
	Arqueologia=21,
	Biologia=22,
	Medicina=23,
	Tecnologia=24,

	Letras=30,
    Literatura=31,
    Arte=32,
    Musica=33
};

enum class Genero
{
	Hombre,
	Mujer
};

class Stoppable
{
    std::promise<void> exitSignal;
    std::future<void> futureObj;
public:
    Stoppable() :
        futureObj(exitSignal.get_future())
    {
    }
    Stoppable(Stoppable&& obj) : exitSignal(std::move(obj.exitSignal)), futureObj(std::move(obj.futureObj))
    {
        std::cout << "Move Constructor is called" << std::endl;
    }
    Stoppable& operator=(Stoppable&& obj)
    {
        std::cout << "Move Assignment is called" << std::endl;
        exitSignal = std::move(obj.exitSignal);
        futureObj = std::move(obj.futureObj);
        return *this;
    }

    virtual void run() = 0;

    void operator()()
    {
        run();
    }

    bool stopRequested()
    {
        if (futureObj.wait_for(std::chrono::milliseconds(0)) == std::future_status::timeout)
            return false;
        return true;
    }

    void stop()
    {
        exitSignal.set_value();
    }
};

class Semaphore {
public:
    Semaphore (int count_ = 0)
    : count(count_), waiting(false)
    {
    }

    inline void notify( int tid ) {
        std::unique_lock<std::mutex> lock(mtx);
        count++;
        //notify the waiting thread
        cv.notify_one();
    }
    inline void wait( int tid ) {
        std::unique_lock<std::mutex> lock(mtx);
        while(count == 0) {
            //wait on the mutex until notify is called
            waiting = true;
            cv.wait(lock);
            waiting = false;
        }
        count--;
    }
    inline bool isWaiting(){
        return waiting;
    }
private:
    std::mutex mtx;
    std::condition_variable cv;
    int count;
    bool waiting;
};

static std::vector<std::string> split(std::string s, std::string delimiter)
{
    size_t pos = 0;
    std::string token;
    std::vector<std::string> splited;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        splited.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    splited.push_back(s);

    return splited;
};

static std::list<std::string> toBuffer(std::string item)
{
    std::list<std::string> temp;
    temp.push_back(item);
    return temp;
}
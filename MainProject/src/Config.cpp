//
// Created by Jorge on 10/07/2021.
//

#include "Config.h"

Config::Config(std::string file)
{
    std::ifstream input(file);
    if(!input)
        std::cerr << "Error al cargar la configuración\n";
    else
    {
        std::vector<std::string> parametros;
        std::string linea;
        std::string separador = "=";
        while(std::getline(input,linea))
        {
            linea.erase(0, linea.find(separador) + separador.length());
            parametros.push_back(linea);
        }
        Config::usuarios = stoi(parametros[0]);
        Config::maxEventos = stoi(parametros[1]);
        Config::tiempoSim = stoi(parametros[2]);
        Config::actividadMedia = stof(parametros[3]);
        Config::etiquetasMedias = stoi(parametros[4]);
        Config::probabilidadEvento = stof(parametros[5]);
        Config::url = parametros[6];
        Config::url.pop_back();
        Config::ip = parametros[7];
        Config::ip.pop_back();
        Config::tiempoEntEvento = stoi(parametros[8]);
        Config::factorDisminucion = stoi(parametros[9]);
        Config::segundosEntreMediciones = stoi(parametros[10]);
    }
}

Config::Config(Config& orig)
{
    Config::usuarios = orig.usuarios;
    Config::maxEventos = orig.maxEventos;
    Config::tiempoSim = orig.tiempoSim;
    Config::actividadMedia = orig.actividadMedia;
    Config::etiquetasMedias = orig.etiquetasMedias;
    Config::probabilidadEvento = orig.probabilidadEvento;
    Config::url = orig.url;
    Config:ip = orig.ip;
    Config::tiempoEntEvento = orig.tiempoEntEvento;
    Config::factorDisminucion = orig.factorDisminucion;
    Config::segundosEntreMediciones = orig.segundosEntreMediciones;
}

int Config::getUsuarios() const {
    return usuarios;
}

void Config::setUsuarios(int usuarios) {
    Config::usuarios = usuarios;
}

int Config::getMaxEventos() const {
    return maxEventos;
}

void Config::setMaxEventos(int maxEventos) {
    Config::maxEventos = maxEventos;
}

int Config::getTiempoSim() const {
    return tiempoSim;
}

void Config::setTiempoSim(int tiempoSim) {
    Config::tiempoSim = tiempoSim;
}

float Config::getActividadMedia() const {
    return actividadMedia;
}

void Config::setActividadMedia(float actividadMedia) {
    Config::actividadMedia = actividadMedia;
}

int Config::getEtiquetasMedias() const {
    return etiquetasMedias;
}

void Config::setEtiquetasMedias(int etiquetasMedias) {
    Config::etiquetasMedias = etiquetasMedias;
}

float Config::getProbabilidadEvento() const {
    return probabilidadEvento;
}

void Config::setProbabilidadEvento(float probabilidadEvento) {
    Config::probabilidadEvento = probabilidadEvento;
}

const std::string &Config::getUrl() const {
    return url;
}

void Config::setUrl(const std::string &url) {
    Config::url = url;
}

const std::string &Config::getIp() const {
    return ip;
}

void Config::setIp(const std::string &ip) {
    Config::ip = ip;
}

int Config::getTiempoEntEvento() const {
    return tiempoEntEvento;
}

void Config::setTiempoEntEvento(int tiempoEntEvento) {
    Config::tiempoEntEvento = tiempoEntEvento;
}

int Config::getFactorDisminucion() const {
    return factorDisminucion;
}

void Config::setFactorDisminucion(int factorDisminucion) {
    Config::factorDisminucion = factorDisminucion;
}

int Config::getSegundosEntreMediciones() const {
    return segundosEntreMediciones;
}

void Config::setSegundosEntreMediciones(int segundosEntreMediciones) {
    Config::segundosEntreMediciones = segundosEntreMediciones;
}

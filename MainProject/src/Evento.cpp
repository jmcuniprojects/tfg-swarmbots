#include "Evento.h"
#include "GestorEventos.h"

Evento::Evento(GestorEventos *g, std::default_random_engine seed, int factorDisminucion, ThreadBuffer<std::list<std::string>> *Log):
    gestor(g),
    finalizado(false),
    seed(rd()),
    dist_cien(0, 100),
    dist_cuatro(0, 3),
    factorDisminucion(factorDisminucion),
    log(Log)
{
}

Evento::~Evento()
{
}

void Evento::ejecutaEvento()
{
	this->hiloEjecucion = std::thread([&]()
	{
		this->run();
	});
}

void Evento::run()
{
    std::list<std::string> logEvento;
    try
    {
        logEvento.push_back(std::to_string(this->id) + ": evento iniciado");
        std::stack<Usuario*> pila;
        //Otra pila para ver a quién contestan
        std::stack<Usuario*> pilaHerencia;
        //Agrego los usuarios base a la cola
        for(auto & usuario : participantes)
        {
            pila.push(usuario);
            pilaHerencia.push(nullptr);
        }
        //Itero en la cola de forma indefinida hasta que se decida parar o se pase el tiempo
        while(!pila.empty())
        {
            if(this->stopRequested())
            {
                break;
            }
            Usuario* usu(pila.top());

            //Check si interectua el usuario
            bool interaccion = false;
            float penalizacion = 0.0f;
            float probInter = ((float)this->dist_cien(seed));
            if(probInter/100 <= this->actividad)
                interaccion = true;

            if(interaccion)
            {
                //En caso positivo se mira que tipo de interaccion se hará
                if(usu->getPerfil())
                {
                    //Si es true sera activo por lo que hará un post, si es una respuesta se mira en la pila de herencia.
                    if(pilaHerencia.top() != nullptr)
                    {
                        usu->post("Evento: " + std::to_string(this->id) + "\n Respuesta a: " + pilaHerencia.top()->getCorreo());
                        logEvento.push_back(std::to_string(this->id) + " || " + usu->getCorreo() + " postea una respuesta a " + pilaHerencia.top()->getCorreo());
                    } else
                    {
                        usu->post("Evento: " + std::to_string(this->id) + "\n Post");
                        logEvento.push_back(std::to_string(this->id) + " || " + usu->getCorreo() + " postea");
                    }
                } else
                {
                    //Si es false sera inactivo por lo que solo hace rt y aplica penalizador a sus seguidores, misma logica en herencia
                    if(pilaHerencia.top() != nullptr)
                    {
                        usu->post("Evento: " + std::to_string(this->id) + "\n RT a: " + pilaHerencia.top()->getCorreo());
                        logEvento.push_back(std::to_string(this->id) + " || " + usu->getCorreo() + " hace rt una respuesta de " + pilaHerencia.top()->getCorreo());
                    } else
                    {
                        //Al ser original no hay post al que hacer RT por lo que se considera post normal pero con penalizador aplicado
                        usu->post("Evento: " + std::to_string(this->id) + "\n Post");
                        logEvento.push_back(std::to_string(this->id) + " || " + usu->getCorreo() + " postea (penalizado)");
                    }
                    penalizacion += 0.5f;
                }
                //Disminuimos la actividad según un factor de interacciones
                float resta = (1.0f/(float)this->factorDisminucion);
                this->actividad = this->actividad - resta;
                this->numPosts++;
            }

            pila.pop();
            pilaHerencia.pop();

            if(this->stopRequested())
            {
                break;
            }

            if(interaccion)
            {
                //Seleccion de nuevos actores
                for (auto & usuario : usu->getSeguidores())
                {
                    if(usuario->findGusto(this->etiquetas))
                    {
                        if(this->dist_cien(seed) > this->actividad * (1 - penalizacion))
                        {
                            pila.push(usuario);
                            pilaHerencia.push(usu);
                        }
                    } else if(penalizacion < 0.5)
                    {
                        if(this->dist_cien(seed) > this->actividad * (1 - 0.75))
                        {
                            pila.push(usuario);
                            pilaHerencia.push(usu);
                        }
                    }
                }
            }
        }
    }catch (std::exception &e){
        logEvento.push_back("Error en evento " + std::to_string(this->id));
    }
    this->finalizado = true;
    logEvento.push_back(std::to_string(this->id) + ": evento terminado con " + std::to_string(this->numPosts) + " interacciones");
    this->guardaEvento(logEvento);
}

void Evento::join()
{
	this->hiloEjecucion.join();
}

void Evento::guardaEvento(std::list<std::string>& registro)
{
    //std::cout << "Escribiendo en buffer...\n";
    this->log->write_buffer(registro);
    //std::cout << "Buffer escrito!\n";
}

//--------------------------- Gets y Sets ----------------------------------//

int Evento::getId()
{
	return this->id;
}

void Evento::setId(int nuevo)
{
	this->id = nuevo;
}

std::list<Etiquetas> Evento::getEtiquetas()
{
	return std::list<Etiquetas>(this->etiquetas);
}

float Evento::getActividad()
{
	return this->actividad;
}

void Evento::setActividad(float nuevo)
{
	this->actividad = nuevo;
}

std::list<Usuario*> Evento::getParticipantes()
{
	return std::list<Usuario*>(this->participantes);
}

int Evento::getNumPosts()
{
	return this->numPosts;
}

void Evento::setNumPosts(int nuevo)
{
	this->numPosts = nuevo;
}

bool Evento::haTerminado()
{
    return Evento::finalizado;
}

//--------------------------------------- Metodos de las listas --------------------------------------//

bool Evento::addEtiqueta(Etiquetas nuevo, bool check)
{
	if (check)
	{
		if (std::find(this->etiquetas.begin(), this->etiquetas.end(), nuevo) != this->etiquetas.end())
		{
			return false;
		}
	}
	this->etiquetas.push_back(nuevo);
	return true;
}

bool Evento::deleteEtiqueta(Etiquetas nuevo)
{
	int preDelete = this->etiquetas.size();
	this->etiquetas.remove(nuevo);
	return this->etiquetas.size() != preDelete;
}

bool Evento::deleteEtiqueta(int pos)
{
	if (pos > this->etiquetas.size())
		return false;
	auto ite = this->etiquetas.begin();
	for (int i = 0; i < pos; i++)
	{
		ite++;
	}
	this->etiquetas.erase(ite);
	return true;
}

std::list<Etiquetas> Evento::resetEtiquetas()
{
	std::list<Etiquetas> copia = std::list<Etiquetas>(this->etiquetas);
	this->etiquetas.clear();
	return copia;
}

bool Evento::addParticipante(Usuario* nuevo, bool check)
{
	if (check)
	{
		if (std::find(this->participantes.begin(), this->participantes.end(), nuevo) != this->participantes.end())
		{
			return false;
		}
	}
	this->participantes.push_back(nuevo);
	return true;
}

bool Evento::deleteParticipante(Usuario* nuevo)
{
	int preDelete = this->participantes.size();
	this->participantes.remove(nuevo);
	return this->participantes.size() != preDelete;
}

bool Evento::deleteParticipante(int pos)
{
	if (pos > this->participantes.size())
		return false;
	auto ite = this->participantes.begin();
	for (int i = 0; i < pos; i++)
	{
		ite++;
	}
	this->participantes.erase(ite);
	return true;
}

std::list<Usuario*> Evento::resetParticipantes()
{
	std::list<Usuario*> copia = std::list<Usuario*>(this->participantes);
	this->participantes.clear();
	return copia;
}
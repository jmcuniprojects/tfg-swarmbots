//
// Created by Jorge on 21/07/2021.
//

#include "GestorEventos.h"

using std::this_thread::sleep_for;

GestorEventos::GestorEventos(Config &conf, std::vector<Usuario*> &usu, int eventosMax, std::default_random_engine seed, ThreadBuffer<std::list<std::string>> *Log):
        seed(seed),
        dist_cien(0, 100),
        dist_cuatro(0, 3),
        idEventos(0),
        configuracion(conf),
        usuarios(usu),
        maxEventos(eventosMax),
        log(Log),
        mediaInteracciones(0),
        totalEventos(0){}

void GestorEventos::run() {
    log->write_buffer(toBuffer("Iniciando simulacion"));
    while (true)
    {
        if (this->stopRequested())
        {
            break;
        }
        if(dist_cien(seed) < configuracion.getProbabilidadEvento()*100)
        //if(eventos.size() < 1)
        {
            try
            {
                int etiqueta = -1;
                //Determinar el tema
                switch (int i = dist_cuatro(seed)) {
                    case 0:
                        //std::cout << "Politica" << "\n";
                        etiqueta = static_cast<int>(Etiquetas::Politica);
                        break;
                    case 1:
                        //std::cout << "Ocio" << "\n";
                        etiqueta = static_cast<int>(Etiquetas::Ocio);
                        break;
                    case 2:
                        //std::cout << "Ciencia" << "\n";
                        etiqueta = static_cast<int>(Etiquetas::Ciencias);
                        break;
                    case 3:
                        //std::cout << "Letras" << "\n";
                        etiqueta = static_cast<int>(Etiquetas::Letras);
                        break;
                }

                int aleatorio = GestorEventos::dist_cien(seed);
                int i = 0;
                int toca = etiqueta/10;
                while(i < 5)
                {
                    if( probEtiquetas[toca][i] <= aleatorio && aleatorio < probEtiquetas[toca][i+1] )
                    {
                        etiqueta += i;
                        etiqueta++;
                        break;
                    }
                    i++;
                }
                auto realEtiqueta = Etiquetas(etiqueta);

                if(maxEventos == eventos.size())
                {
                    log->write_buffer(toBuffer("Limite excedido"));
                    auto listoBorrar = eventos.end();
                    float minActividad = (*eventos.begin())->getActividad();
                    auto older = eventos.begin();
                    for(auto ite = eventos.begin(); ite != eventos.end(); ite++)
                    {
                        if((*ite)->haTerminado())
                        {
                            listoBorrar = ite;
                            break;
                        } else if(minActividad > (*ite)->getActividad()){
                            older = ite;
                        }
                    }
                    if(listoBorrar != eventos.end())
                    {
                        (*listoBorrar)->join();
                        this->mediaInteracciones += (*listoBorrar)->getNumPosts();
                        this->totalEventos++;
                        delete(*listoBorrar);
                        eventos.erase(listoBorrar);
                        log->write_buffer(toBuffer( "Evento borrado por finalizacion"));
                    } else if(dist_cien(seed) >= 50){
                        (*older)->stop();
                        (*older)->join();
                        this->mediaInteracciones += (*older)->getNumPosts();
                        this->totalEventos++;
                        delete(*older);
                        eventos.erase(older);
                        log->write_buffer(toBuffer("Evento borrado por extension"));
                    }
                }

                if(maxEventos > eventos.size())
                {
                    log->write_buffer(toBuffer("Evento en creacion"));
                    auto *temp = new Evento(this, this->seed, GestorEventos::configuracion.getFactorDisminucion(), this->log);
                    temp->setId(idEventos);
                    idEventos++;
                    int actividad = dist_cien(seed);
                    temp->setActividad(((float)actividad)/100);
                    temp->setNumPosts(0);
                    temp->addEtiqueta(realEtiqueta,false);

                    for(auto & usuario : usuarios)
                    {
                        if(usuario->findGusto(realEtiqueta))
                        {
                            if(dist_cien(seed) <= actividad)
                            {
                                temp->addParticipante(usuario, false);
                            }
                        }
                    }

                    if(!temp->getParticipantes().empty())
                    {
                        eventos.push_back(temp);
                        temp->ejecutaEvento();
                    } else {
                        GestorEventos::idEventos--;
                        delete(temp);
                    }
                    if (this->stopRequested())
                    {
                        break;
                    }
                }
            }catch (std::exception &e){
                log->write_buffer(toBuffer("Error en el gestor"));
            }
        }
        sleep_for(std::chrono::seconds(configuracion.getTiempoEntEvento()));
    }

    for(auto & evento : eventos)
    {
        evento->stop();
    }
    for(auto & evento : eventos)
    {
        evento->join();
        this->mediaInteracciones += evento->getNumPosts();
        this->totalEventos++;
        delete(evento);
    }
    eventos.clear();
    log->write_buffer(toBuffer("Finalización correcta"));
}

float GestorEventos::getMediaInteracciones() {
    return mediaInteracciones/totalEventos;
}

float GestorEventos::getTotalInteracciones() {
    return mediaInteracciones;
}

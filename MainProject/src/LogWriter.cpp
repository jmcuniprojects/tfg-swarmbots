#include "LogWriter.h"

LogWriter::LogWriter(ThreadBuffer<std::list<std::string>>* buffer, std::string fileNames):
    fileName(fileNames),
    buffer(buffer)
{
}

void LogWriter::run(){
    this->outputFile.open(this->fileName);
    while(true){
        if(this->stopRequested())
        {
            break;
        }
        std::list<std::string> log;
        if(!this->buffer->read_buffer(log)){
            break;
        }
        //std::cout << "Escribiendo...\n";
        for(auto & msg : log){
            this->outputFile << msg << "\n";
        }
    }
    this->outputFile.close();
    //std::cout << "Escritor out\n";
}
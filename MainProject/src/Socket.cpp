#include "socket.h"

#define PORT "9082"

using namespace std;
using namespace serversock;

int sockfd, n;
struct sockaddr_in serv_addr;

void serversock::createConnection(std::string IP) {

    /* Create a socket point */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) {
        perror("ERROR abriendo conexion");
        exit(1);
    } else if (sockfd > 0) {
        cout << "Abriendo conexion" << endl;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(PORT));
    inet_pton(AF_INET, IP.c_str(), &(serv_addr.sin_addr.s_addr));

    cout << "conectando al servidor..." << endl;

    int conn_success = connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

    if (conn_success < 0) {
        perror("ERROR conexion no establecida");
    } else {
        cout << "conexion correcta" << endl;
    }
}

string serversock::receive(int buffer_size)
{
    char buffer[buffer_size];
    n = recv(sockfd, buffer, sizeof(buffer), 0);
    if(n > buffer_size)
    {
        std::cerr << "WARNING: Buffer insuficiente\n";
    }
    string msg = "";
    for(int i = 0; i < n; i++)
    {
        msg += buffer[i];
    }
    return msg;
}

void serversock::sending(string msg)
{
    send(sockfd , msg.c_str() , strlen(msg.c_str()) , 0 );
}
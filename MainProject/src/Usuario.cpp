#include "Usuario.h"

Usuario::Usuario() :
	edad(0),
	id(0),
	correo(""),
	pass(""),
	importancia(0),
	perfil(false),
	instancia(MASTODON_URL,""),
	tokenAcceso("")
{
	this->gen = Genero();
	this->seguidores = std::list<Usuario*>();
	this->gustos = std::list<Etiquetas>();

	this->conexion = nullptr;
}

Usuario::Usuario(std::string token):
	edad(0),
	id(0),
	correo(""),
	pass(""),
	importancia(0),
	perfil(false),
	instancia(MASTODON_URL,token),
	tokenAcceso(token)
{
	this->gen = Genero();
	this->seguidores = std::list<Usuario*>();
	this->gustos = std::list<Etiquetas>();

	this->conexion = new mastodonpp::Connection(this->instancia);
}


Usuario::~Usuario()
{
	if(this->conexion != nullptr)
		delete this->conexion;
}

//--------------------------- Mastodon functions ----------------------------------//

bool Usuario::registrar()
{
	mastodonpp::Instance::ObtainToken token{this->instancia};
	auto answer = token.step_1("SocialBench", "read write follow", "");
	if (answer)
	{
		std::cout << "Please visit " << answer << " and paste the code:\n";
		std::string code;
		std::cin >> code;
		answer = token.step_2(code);
		if (answer)
		{
			if(this->conexion != nullptr)
				delete this->conexion;
			this->conexion = new mastodonpp::Connection(this->instancia);
			this->tokenAcceso = this->instancia.get_access_token();
			return true;
		}
	}
	return false;
}

bool Usuario::post(std::string msg)
{
	if(this->conexion != nullptr)
	{
		auto answer = this->conexion->post(mastodonpp::API::v1::statuses,{{"status", msg.c_str()}});
		if(answer)
		{
			return true;
		}
	}
	return false;
}

//--------------------------- Gets y Sets ----------------------------------//

Genero Usuario::getGenero()
{
	return this->gen;
}

void Usuario::setGenero(Genero nuevo)
{
	this->gen = nuevo;
}

int Usuario::getEdad()
{
	return this->edad;
}

void Usuario::setEdad(int nuevo)
{
	this->edad = nuevo;
}

uint16_t Usuario::getId()
{
	return this->id;
}

void Usuario::setId(uint16_t nuevo)
{
	this->id = nuevo;
}

std::string Usuario::getCorreo()
{
	return this->correo;
}

void Usuario::setCorreo(std::string nuevo)
{
	this->correo = nuevo;
}

std::string Usuario::getPass()
{
	return this->pass;
}

bool Usuario::setPass(std::string nuevo, std::string old)
{
	if (old.compare(this->pass) == 0)
	{
		this->pass = nuevo;
		return true;
	}
	return false;
}

float Usuario::getImportancia()
{
	return this->importancia;
}

void Usuario::setImportancia(float nuevo)
{
	this->importancia = nuevo;
}

std::list<Usuario*> Usuario::getSeguidores()
{
	return std::list<Usuario*>(this->seguidores);
}

bool Usuario::getPerfil()
{
	return this->perfil;
}
void Usuario::setPerfil(bool nuevo)
{
	this->perfil = nuevo;
}

std::list<Etiquetas> Usuario::getGustos()
{
	return std::list<Etiquetas>(this->gustos);
}

//--------------------------------------- M�todos de las listas --------------------------------------//

bool Usuario::addSeguidor(Usuario* nuevo, bool check)
{
	if (check)
	{
		if (std::find(this->seguidores.begin(), this->seguidores.end(), nuevo) != this->seguidores.end())
		{
			return false;
		}
	}
	this->seguidores.push_back(nuevo);
	return true;
}

bool Usuario::deleteSeguidor(Usuario* nuevo)
{
	int preDelete = this->seguidores.size();
	this->seguidores.remove(nuevo);
	return !(this->seguidores.size() == preDelete);
}

bool Usuario::deleteSeguidor(int pos)
{
	if (pos > this->seguidores.size())
		return false;
	std::list<Usuario*>::iterator ite = this->seguidores.begin();
	for (int i = 0; i < pos; i++)
	{
		ite++;
	}
	this->seguidores.erase(ite);
	return true;
}

std::list<Usuario*> Usuario::resetSeguidores()
{
	std::list<Usuario*> copia = std::list<Usuario*>(this->seguidores);
	this->seguidores.clear();
	return copia;
}

bool Usuario::addGusto(Etiquetas nuevo, bool check)
{
    if (check)
    {
        if (std::find(this->gustos.begin(), this->gustos.end(), nuevo) != this->gustos.end())
        {
            return false;
        }
    }
	this->gustos.push_back(nuevo);
	return true;
}

bool Usuario::deleteGusto(Etiquetas nuevo)
{
	int preDelete = this->gustos.size();
	this->gustos.remove(nuevo);
	return !(this->gustos.size() == preDelete);
}

bool Usuario::deleteGusto(int pos)
{
	if (pos > this->gustos.size())
		return false;
	auto ite = this->gustos.begin();
	for (int i = 0; i < pos; i++)
	{
		ite++;
	}
	this->gustos.erase(ite);
	return true;
}

std::list<Etiquetas> Usuario::resetGustos()
{
	std::list<Etiquetas> copia = std::list<Etiquetas>(this->gustos);
	this->gustos.clear();
	return copia;
}

void Usuario::newToken(std::string token)
{
    this->tokenAcceso = token;
    this->instancia.set_access_token(token);
    if(this->conexion != nullptr)
        delete(this->conexion);
    this->conexion = new mastodonpp::Connection(this->instancia);
}

bool Usuario::findGusto(Etiquetas f) {
    if (std::find(this->gustos.begin(), this->gustos.end(), f) != this->gustos.end())
    {
        return true;
    }
    return false;
}

bool Usuario::findGusto(std::list<Etiquetas> f) {
    for(Etiquetas & etiqueta : f)
    {
        if (std::find(this->gustos.begin(), this->gustos.end(), etiqueta) != this->gustos.end())
        {
            return true;
        }
    }
    return false;
}

std::string Usuario::getToken() {
    return tokenAcceso;
}

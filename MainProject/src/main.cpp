﻿// SocialBench.cpp: define el punto de entrada de la aplicación.

#include <iostream>
#include <thread>
#include <chrono>
#include <list>
#include <fstream>

#include "LogWriter.h"
#include "GestorEventos.h"
#include "ThreadBuffer.h"
#include "Socket.h"
#include "json.h"

using std::cout;
using std::cin;
using std::endl;
using std::this_thread::sleep_for;

void generaEtiquetas(Usuario* temp, int numEtiquetas, std::default_random_engine &seed, std::uniform_int_distribution<int> &dist_cien, std::uniform_int_distribution<int> &dist_ETI)
{
    // Definición de variables probabilisticas
    int probabilidadPolitica = 50;
    int minimoMultCategoria = 4;

    if( dist_cien(seed) < probabilidadPolitica )
    {
        int orientacion = dist_cien(seed);
        for(int i = 0; i < 5 ; i++)
        {
            if( probEtiquetas[0][i] < orientacion && orientacion < probEtiquetas[0][i+1] )
            {
                temp->addGusto(Etiquetas(i+1+static_cast<int>(Etiquetas::Politica)), false);
            }
        }
        numEtiquetas--;
    }

    // Generación de distribución de etiquetas por grupos
    int probabilidades[] = {0,0,0,100};
    int mod;
    if(numEtiquetas < minimoMultCategoria)
    {
        mod = 100;
    }
    else
    {
        mod = 90;
    }
    int acumulada = 0;
    int i = 1;
    while(i < 3 && acumulada < 100)
    {
        int p = (dist_cien(seed) % mod);
        if(p + acumulada < 100)
            probabilidades[i] = acumulada + p;
        else
            probabilidades[i] = 100;
        acumulada += p;
        i++;
    }
    if(acumulada < 100) {
        int dif = 100 - acumulada;
        int sum = dif / 3;
        for (int j = 1; j < 3; j++) {
            probabilidades[j] += sum;
        }
    }

    // Establezco el orden de asignacion de etiquetas aleatorio
    int ordenEtiquetas[3];
    int et = dist_ETI(seed) + 1;
    for( i = 0; i < 3; i++)
    {
        ordenEtiquetas[i] = et*10;
        et = (et+1);
        if(et > 3)
            et = 1;
    }

    // Asingo etiquetas según el orden
    for(i = 0; i < 3; i++)
    {
        int porcentaje = probabilidades[i+1] - probabilidades[i];
        if(porcentaje > 0)
        {
            int etiquetas = ((float)numEtiquetas * ((float)porcentaje/100));
            int toca = ordenEtiquetas[i]/10;
            int ite = 0;
            while(etiquetas > 0)
            {
                int random = dist_cien(seed);
                int j = 0;
                bool repeat = false;
                while(true)
                {
                    if(probEtiquetas[toca][j] <= random && random <= probEtiquetas[toca][j+1])
                    {
                        repeat = !temp->addGusto(Etiquetas(j+1+ordenEtiquetas[i]), true);
                        break;
                    }
                    j++;
                }
                if(!repeat || ite > 50) {
                    etiquetas--;
                    ite = 0;
                }
                else
                    ite++;
            }
        }
    }
}

void generaUsuarios(std::vector<Usuario*> &usuarios, Config &configFile, std::default_random_engine &seed, std::uniform_int_distribution<int> &dist_cien)
{
  std::ifstream in(USERPASS);
  int counter = 0;
  std::vector<std::list<int>> seguidores;
  std::uniform_int_distribution<int> dist_ETI(0, OSCILACION_ETI);
  std::uniform_int_distribution<int> dist_edad(11,70);
  std::uniform_int_distribution<int> dist_Usuarios(0, configFile.getUsuarios()-1);

  if(in)
  {
      for(int i = 0; i < configFile.getUsuarios(); i++)
      {
          auto* temp = new Usuario();
          if(in.eof() == 0) {
              std::string userData;
              std::getline(in, userData);
              userData.pop_back();

              if (dist_cien(seed) <= 50) {
                  temp->setGenero(Genero::Mujer);
              } else {
                  temp->setGenero(Genero::Hombre);
              }

              temp->setEdad(dist_edad(seed) + 5);
              temp->setId(i);

              std::vector<std::string> splited = split(userData, ";");
              temp->setCorreo(splited[0]);
              temp->setPass(splited[1], "");
              if(splited[2].compare(""))
              {
                  temp->newToken(splited[2]);
              } else {
                  std::ofstream keys;
                  keys.open("keys.txt");

                  std::cout << "Por favor autorice a la herramienta en la red para el usuario: " << temp->getCorreo() << "\n";
                  temp->registrar();
                  keys << ";" << temp->getToken() << "\n";

                  keys.close();
              }

              float imp = (dist_cien(seed) + 1);
              imp = imp / 100;
              if (imp < 0.7) {
                  if ((imp - 0.2) > 0.0) {
                      imp = imp - 0.2;
                      temp->setImportancia(imp);
                  } else {
                      temp->setImportancia(imp);
                  }
              } else {
                  temp->setImportancia(imp);
              }

              int numSeguidores = (imp * configFile.getUsuarios());
              if(numSeguidores >= configFile.getUsuarios()-2 )
                  numSeguidores = configFile.getUsuarios()-2;
              seguidores.push_back(std::list<int>());
              while (numSeguidores != 0) {
                  int r;
                  do {
                      r = dist_Usuarios(seed);
                  }while( r == i || std::find(seguidores[i].begin(), seguidores[i].end(), r) != seguidores[i].end());

                  seguidores[i].push_back(r);
                  numSeguidores = numSeguidores - 1;
              }

              if (dist_cien(seed) <= configFile.getActividadMedia() * 100) {
                  temp->setPerfil(false);
              } else {
                  temp->setPerfil(true);
              }

              int numEtiquetas = configFile.getEtiquetasMedias();
              float et = (dist_cien(seed) + 1);
              et = et / 100;
              if (et < 0.7) {
                  if (et < 0.3) {
                      int minusEtiquetas = (dist_ETI(seed) + 1);
                      numEtiquetas = numEtiquetas - minusEtiquetas;
                  }
              } else {
                  int plusEtiquetas = (dist_ETI(seed) + 1);
                  numEtiquetas += plusEtiquetas;
              }
              generaEtiquetas(temp, numEtiquetas, seed, dist_cien, dist_ETI);

              usuarios.push_back(temp);
              counter++;
          }
      }
      for(int i = 0; i < counter; i++)
      {
          for(std::list<int>::iterator it1 = seguidores[i].begin(); it1 != seguidores[i].end(); it1++)
            usuarios[i]->addSeguidor(usuarios[*it1],false);
      }
  }
}

void borraRecursos(std::vector<Usuario*> &usuarios)
{
    for(int i = 0; i < usuarios.size(); i++)
    {
        delete(usuarios[i]);
    }
    usuarios = std::vector<Usuario*>();
}

int main()
{
    std::random_device rd;
    std::default_random_engine seed(rd());
    std::uniform_int_distribution<int> dist_cien(0, 100);
    std::uniform_int_distribution<int> dist_cuatro(0, 4);

    Config configuracion = Config("config.txt");
    std::vector<Usuario*> usuarios;
    generaUsuarios(usuarios, configuracion, seed, dist_cien);
    ThreadBuffer<std::list<std::string>> buffer;

    serversock::createConnection(configuracion.getIp());
    std::cout << serversock::receive(10) << "\n";
    int iters = (configuracion.getTiempoSim()*60)/configuracion.getSegundosEntreMediciones();
    int wait_time = configuracion.getSegundosEntreMediciones();
    serversock::sending(std::to_string(iters) + ";" + std::to_string(wait_time));

    GestorEventos gestor(configuracion, usuarios, configuracion.getMaxEventos(), seed, &buffer);
    std::thread hiloGestor = std::thread([&]()
    {
        gestor.run();
    });
    std::cout << "Gestor lanzado\n";
    sleep_for(std::chrono::seconds(5));
    LogWriter escritor(&buffer, "log.txt");
    std::thread hiloEscritor = std::thread([&]()
    {
        escritor.run();
    });
    std::cout << "Escritor lanzado\n";

    sleep_for(std::chrono::minutes(configuracion.getTiempoSim()));

    gestor.stop();
    hiloGestor.join();
    std::cout << "Gestor finalizado\n";
    float mediaInteracciones = gestor.getMediaInteracciones();
    float totalInteracciones = gestor.getTotalInteracciones();
    while(!buffer.notify_empty()){
        std::cout << "Esperando escritor...\n";
        sleep_for(std::chrono::seconds(5));
    }
    hiloEscritor.join();
    std::cout << "Escritor finalizado\n";
    nlohmann::json j = nlohmann::json::parse(serversock::receive((148*iters)+5000));

    borraRecursos(usuarios);

    std::vector<float> lectura_CPU;
    std::vector<float> lectura_RAM;
    std::vector<std::vector<float>> lectura_Disco;
    int i = 0;
    for(auto & item : j["time_log"].get<std::list<nlohmann::json>>())
    {
        lectura_CPU.push_back(item["cpu_usage"]);
        lectura_RAM.push_back(item["memory_usage"]);
        lectura_Disco.push_back(std::vector<float>());
        for(auto & item2 : item["disk_usage"].get<std::map<std::string, float>>())
        {
            lectura_Disco[i].push_back(item2.second);
        }
        i++;
    }

    std::ofstream informe;
    informe.open("Informe.csv");
    informe << "Iteracion;Uso CPU;Uso RAM;Numero de Lecturas;Numero de escrituras;Tiempo de lectura;Tiempo de escritura\n";
    for(int h = 0; h < lectura_CPU.size(); h++)
    {
        informe << h << ";" << lectura_CPU[h] << ";" << lectura_RAM[h];
        for(int k = 0; k < lectura_Disco[h].size(); k++)
        {
            informe << ";" << lectura_Disco[h][k];
        }
        informe << "\n";
    }
    informe << "\n\nMedia de Interacciones;Interacciones Totales\n" << mediaInteracciones << ";" << totalInteracciones;
    informe.close();

    return 0;
}

default:
	@cd build && make
	@mv build/MainProject/src/SocialBench SocialBench

cmake: build
	@cd build && cmake ..

cmakeDebug: build
	@cd build && cmake -DCMAKE_BUILD_TYPE=Debug ..

build:
	@mkdir build

clean:
	rm SocialBench
	rm  -r build/*
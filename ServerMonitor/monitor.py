import psutil
import time
import json
import socket
import struct

def generaLog(iteraciones, espera):
    registro=dict()
    registroStandar=dict()
    f = open("serverLog.txt", "w")

    registroStandar["cpu_cores"] = psutil.cpu_count()
    f.write("CPU logical cores: "+ str(registroStandar["cpu_cores"])+"\n")
    registroStandar["total_memory"] = psutil.virtual_memory().total/(1024*1024)
    f.write("Total memory: "+ str(int(registroStandar["total_memory"]))+ "MBs\n")
    registroStandar["total_disk"] = psutil.disk_usage("/").total/(1024*1024*1024)
    f.write("Total disk usage: "+ str(int(registroStandar["total_disk"]))+ "GBs\n\n")
    counter=iteraciones
    registro["time_log"] = []
    psutil.cpu_percent(interval=None)
    f.write("----------------------------\n")
    for i in range(counter):
        registro["time_log"].append({"cpu_usage": psutil.cpu_percent(interval=None), "memory_usage": psutil.virtual_memory().used/(1024*1024), "disk_usage": {"read_count": psutil.disk_io_counters(nowrap=True).read_count, "write_count": psutil.disk_io_counters(nowrap=True).write_count, "read_time": psutil.disk_io_counters(nowrap=True).read_time, "write_time": psutil.disk_io_counters(nowrap=True).write_time}})
        f.write("CPU usage: "+ str(registro["time_log"][i]["cpu_usage"])+"\n")
        f.write("Memory usage: "+ str(registro["time_log"][i]["memory_usage"])+ "MBs\n")
        f.write("Disk usage: (read_count="+ str(registro["time_log"][i]["disk_usage"]["read_count"])+ ", write_count="+ str(registro["time_log"][i]["disk_usage"]["write_count"])+ ", read_time="+ str(registro["time_log"][i]["disk_usage"]["read_time"])+ ", write_time="+ str(registro["time_log"][i]["disk_usage"]["write_time"])+ ")\n")
        f.write("----------------------------\n")
        time.sleep(espera)
    f.close()

    f = open("registro.json", "w")
    f.write(json.dumps(registro))
    f = open("registroStandar.json", "w")
    f.write(json.dumps(registroStandar))
    f.close()

PORT, HOST_IP = 8082, '127.0.0.1'
key = 4

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST_IP, PORT))
    s.listen()
    print("starting to listen")
    conn, addr = s.accept()
    with conn:
        print('Connected by', addr)
        conn.sendall(b'conectado')
        time.sleep(2)
        data = conn.recv(1024)
        msg = data.decode("utf-8")
        parameters = msg.split(";")
        generaLog(int(parameters[0]),int(parameters[1]))
        f = open("registro.json","r")
        conn.sendall(bytearray(f.read(),"utf-8"))
        time.sleep(4)